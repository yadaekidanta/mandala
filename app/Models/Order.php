<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = "order";
    public function catalog()
    {
        return $this->belongsTo(Catalog::class,'catalog_id','id');
    }
    public function customer()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
