<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog;

class CatalogController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Catalog::where('titles','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.web.catalog.list', compact('collection'));
        }
        return view('page.web.catalog.main');
    }
    public function show(Catalog $catalog)
    {
        $collection = Catalog::where('id','!=',$catalog->id)->get();
        return view('page.web.catalog.show',compact('catalog','collection'));
    }
}
