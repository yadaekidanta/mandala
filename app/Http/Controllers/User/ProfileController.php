<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $token = Auth::user()->id;
            $keywords = $request->keywords;
            $collection = Order::where('user_id',$token)
            ->orderBy('id','DESC')
            ->paginate(10);
            return view('page.web.auth.list', compact('collection'));
        }
        return view('page.web.auth.profile');
    }
    public function edit_profile(User $user)
    {
        return view('page.web.auth.edit', compact('user'));
    }
    public function update_profile(Request $request, User $user){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|max:255',
            'phone' => 'required|min:9|max:15',
            'photo' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('phone')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('phone'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($user->photo);
            $file = request()->file('photo')->store("user");
            $user->photo = $file;
        }
        $user->name = Str::title($request->name);
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->province = $request->province;
        $user->city = $request->city;
        $user->subdistrict = $request->subdistrict;
        $user->postcode = $request->postcode;
        $user->username = Str::before($request->email, '@');
        if($request->password){
            $user->password = Hash::make($request->password);
        }
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Profile Updated',
        ]);
    }
}
