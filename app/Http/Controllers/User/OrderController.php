<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'jam' => 'required',
            'harga' => 'max:19',
            'total_jam' => 'max:1',
            'total_harga' => 'max:19',
            'photo' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }elseif ($errors->has('jam')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jam'),
                ]);
            }elseif ($errors->has('harga')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('harga'),
                ]);
            }elseif ($errors->has('total_jam')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('total_jam'),
                ]);
            }elseif ($errors->has('total_harga')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('total_harga'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        $file = request()->file('photo')->store("order");
        $jam_tutup = "23";
        $jam = $request->jam;
        $jumlah_jam = $request->total_jam;
        $total_jam = $jam + $jumlah_jam;
        if($total_jam <= $jam_tutup){
            $catalog = $request->catalog;
            $tanggal = date_create($request->tanggal);
            $start = date_format($tanggal, "Y-m-d");
            $date = $start.' '.$jam . date(":00:00");
            $end = date_format(Carbon::parse($date)->addHour($jumlah_jam), "Y-m-d H:i:s");
            DB::enableQueryLog();
            $cek = Order::where("catalog_id",$catalog)
            ->where('end','>',$date)
            ->where('start','<',$end)
            // ->whereBetween('start', [$date, date_format(Carbon::parse($end), "Y-m-d H:i:s")])
            // ->orWhereBetween('end', [$date, date_format(Carbon::parse($end), "Y-m-d H:i:s")])
            ->get()->count();
            // dd(DB::getQueryLog());
            // dd($cek);
            $harga = Str::of($request->harga)->replace(',', '')?:0;
            $total_harga = Str::of($request->total_harga)->replace(',', '');
            if($cek > 0){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Tanggal ' .$request->tanggal. ' pada jam ' .$request->jam . ' sudah dipesan',
                ]);
            }else{
                $order = new Order;
                $order->user_id = Auth::user()->id;
                $order->catalog_id = $catalog;
                $order->notes = '';
                $order->start = $date;
                $order->end = $end;
                $order->st = 'Wait for confirmation';
                $order->total = $total_harga;
                $order->photo = $file;
                $order->save();
                return response()->json([
                    'alert' => 'success',
                    'message' => 'Order Created',
                    // 'callback' => '../profile'
                ]);
            }
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Jumlah jam melebihi jam tutup',
            ]);
        }
    }
    public function cancel(Order $order)
    {
        $order->st = 'Cancel';
        $order->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Order has been cancel',
        ]);
    }
}
