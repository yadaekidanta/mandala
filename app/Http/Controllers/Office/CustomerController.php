<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = User::where('role','!=','Admin')
            ->where('name','like','%'.$keywords.'%')
            ->paginate(10);
            return view('page.office.customer.list', compact('collection'));
        }
        return view('page.office.customer.main');
    }
}
