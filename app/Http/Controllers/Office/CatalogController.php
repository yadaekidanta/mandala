<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class CatalogController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Catalog::where('titles','like','%'.$keywords.'%')->paginate(10);
            return view('page.office.catalog.list', compact('collection'));
        }
        return view('page.office.catalog.main');
    }
    public function create()
    {
        return view('page.office.catalog.input', ["catalog" => new Catalog]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'titles' => 'required|unique:catalog',
            'description' => 'required',
            'price_s' => 'max:19',
            'price_m' => 'max:19',
            'photo' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('titles')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('titles'),
                ]);
            }elseif ($errors->has('description')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif ($errors->has('price_s')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price_s'),
                ]);
            }elseif ($errors->has('price_m')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price_m'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('photo'),
                ]);
            }
        }
        $file = request()->file('photo')->store("catalog");
        $catalog = new Catalog;
        $catalog->titles = Str::title($request->titles);
        $catalog->description = Str::title($request->description);
        $catalog->price_s = Str::of($request->price_s)->replace(',', '')?:0;
        $catalog->price_m = Str::of($request->price_m)->replace(',', '')?:0;
        $catalog->slug = Str::slug($request->titles);
        $catalog->photo = $file;
        $catalog->save();
        // Client::create($request->all());
        return response()->json([
            'alert' => 'success',
            'message' => 'Catalog '. $request->titles . ' Saved',
        ]);
    }
    public function show(Catalog $catalog)
    {
        //
    }
    public function edit(Catalog $catalog)
    {
        return view('page.office.catalog.input', compact('product'));
    }
    public function update(Request $request, Catalog $catalog)
    {
        $validator = Validator::make($request->all(), [
            'titles' => 'required',
            'description' => 'required',
            'price_s' => 'max:19',
            'price_m' => 'max:19',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('titles')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('titles'),
                ]);
            }elseif ($errors->has('description')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('description'),
                ]);
            }elseif ($errors->has('price_s')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price_s'),
                ]);
            }elseif ($errors->has('price_m')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('price_m'),
                ]);
            }
        }
        if(request()->file('photo')){
            Storage::delete($catalog->photo);
            $file = request()->file('photo')->store("catalog");
            $catalog->photo = $file;
        }
        $catalog->titles = Str::title($request->titles);
        $catalog->description = Str::title($request->description);
        $catalog->price_s = Str::of($request->price_s)->replace(',', '')?:0;
        $catalog->price_m = Str::of($request->price_m)->replace(',', '')?:0;
        $catalog->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Catalog '. $request->titles . ' Updated',
        ]);
    }
    public function destroy(Catalog $catalog)
    {
        Storage::delete($catalog->photo);
        $catalog->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Catalog '. $catalog->titles . ' Deleted',
        ]);
    }
}
