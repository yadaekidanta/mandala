<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $start = $request->start;
            $end = $request->end;
            $collection = Order::whereBetween(DB::raw('date(created_at)'), [$start, $end])
            ->orderBy('id','DESC')
            ->paginate(10);
            return view('page.office.order.list', compact('collection'));
        }
        return view('page.office.order.main');
    }
    public function reject(Order $order)
    {
        $order->st = "Payment Rejected";
        $order->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Payment rejected',
        ]);
    }
    public function acc(Order $order){
        $order->st = "Payment accepted";
        $order->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Payment accepted',
        ]);
    }
    public function download(Order $order)
    {
        $extension = Str::of($order->photo)->explode('.');
        return Storage::download($order->photo, 'order-'.$order->created_at.'.'.$extension[1]);
    }
}
