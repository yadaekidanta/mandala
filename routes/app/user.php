<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\AuthController;
use App\Http\Controllers\User\CatalogController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\OrderController;
use App\Http\Controllers\User\WebController;

Route::group(['domain' => ''], function() {
    Route::prefix('')->name('user.')->group(function(){
        Route::redirect('/', 'home', 301);
        Route::get('home', [WebController::class, 'index'])->name('home');
        Route::get('catalog', [CatalogController::class, 'index'])->name('catalog.index');
        Route::get('catalog/{catalog:slug}', [CatalogController::class, 'show'])->name('catalog.show');
        Route::get('auth',[AuthController::class, 'index'])->name('auth.index');
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
        });

        Route::middleware(['auth'])->group(function(){
            Route::get('profile',[ProfileController::class, 'index'])->name('auth.profile');
            Route::get('profile/{user:id}/edit',[ProfileController::class, 'edit_profile'])->name('auth.edit');
            Route::post('profile/{user}/update',[ProfileController::class, 'update_profile'])->name('auth.update');
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
            Route::get('order', [OrderC::class, 'index'])->name('order.index');
            Route::post('order/store', [OrderController::class, 'store'])->name('order.store');
            Route::patch('order/{order}/cancel', [OrderController::class, 'cancel'])->name('order.cancel');
        });
    });
});
