<form id="form_profile">
    <div class="row col-mb-50 gutter-50">
        <div class="col-lg-6">
            <h3>Personal Information</h3>
            <div class="row mb-0">
                <div class="col-md-6 form-group">
                    <label for="billing-form-name">Name:</label>
                    <input type="text" name="name" value="{{Auth::user()->name}}" class="sm-form-control" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="billing-form-email">Email Address:</label>
                    <input type="email" name="email" value="{{Auth::user()->email}}" class="sm-form-control" />
                </div>
                <div class="w-100"></div>

                <div class="col-md-6 form-group">
                    <label for="billing-form-phone">Phone:</label>
                    <input type="text" name="phone" value="{{Auth::user()->phone}}" class="sm-form-control" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="billing-form-password">Password:</label>
                    <input type="password" name="password" class="sm-form-control" />
                </div>
                <div class="col-12 form-group">
                    <label for="photo">Photo <small>*</small></label>
                    <input type="file" accept="image/*" name="photo">
                </div>

            </div>
        </div>
        <div class="col-lg-12">
            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_profile','{{route('user.auth.update',$user->id)}}','POST','Update')" class="button button-3d float-end">Update</button>
        </div>
    </div>
</form>