<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Lapangan</th>
            <th style="width:200px;">Tanggal</th>
            <th style="width:200px;">Jam</th>
            <th>Total</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($collection as $item)
        @php
        $total = 0;
        @endphp
        <tr>
            <td>
                <img style="width:10%;" src="{{asset($item->catalog->image)}}" alt="">
                <span class="float-start;">
                    {{$item->catalog->titles}}
                </span>
                <span class="float-end;">
                    {{-- Rp. {{number_format($order_detail->price)}} x {{number_format($order_detail->qty)}} --}}
                </span>
            </td>
            <td>
                <code>
                    {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->start)->format('D, j F Y');}}
                </code>
            </td>
            <td>
                <code>
                    Mulai : {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->start)->format('G:i');}}<br>
                    Sampai : {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->end)->format('G:i');}}
                </code>
            </td>
            <td>{{number_format($item->total)}}</td>
            <td>
                {{$item->st}}
                @if ($item->st == "On the way" || $item->st == "Received")
                <br>
                    Resi :{{$item->resi}}
                @endif
            </td>
            <td>
                @if ($item->st == "Wait for confirmation")
                    <a href="javascript:;" onclick="handle_confirm('{{route('user.order.cancel',$item->id)}}');">Cancel</a>
                @elseif($item->st == "On the way")
                    <a href="javascript:;" onclick="handle_confirm('{{route('user.order.receive',$item->id)}}');">Receive</a>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links()}}