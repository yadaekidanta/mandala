<x-web-layout title="Katalog">
    <section id="page-title">
        <div class="container clearfix">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('user.home')}}">Home</a></li>
                <li class="breadcrumb-item">
                    <a href="{{route('user.catalog.index')}}">Catalog</a>
                </li>
            </ol>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div id="content_list">
                <div id="list_result"></div>
            </div>
        </div>
    </section>
    @section('custom_js')
        <script type="text/javascript">
            load_list(1);
        </script>
    @endsection
</x-web-layout>