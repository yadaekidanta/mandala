<div id="shop" class="shop row grid-container leftmargin-sm" data-layout="fitRows">
    @foreach ($collection as $item)
    <div class="product col-lg-3 col-md-4 col-sm-6 col-12">
        <div class="grid-inner">
            <div class="product-image">
                <a href="{{route('user.catalog.show',$item->slug)}}">
                    <img src="{{$item->image}}" alt="{{$item->titles}}">
                </a>
            </div>
            <div class="product-desc">
                <div class="product-title">
                    <h3>
                        <a href="{{route('user.catalog.show',$item->slug)}}">
                            {{$item->titles}}
                        </a>
                    </h3>
                </div>
                <div class="product-price">
                    <ins>
                        Rp. {{number_format($item->price_s)}} - Rp. {{number_format($item->price_m)}}
                    </ins>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>