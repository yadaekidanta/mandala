<x-web-layout title="{{$catalog->titles}}">
    <section id="page-title">
        <div class="container clearfix">
            <h1>{{$catalog->titles}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('user.home')}}">Home</a></li>
                <li class="breadcrumb-item">
                    <a href="{{route('user.catalog.index')}}">Katalog</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{$catalog->titles}}</li>
            </ol>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="single-product">
                    <div class="product">
                        <div class="row gutter-40">
                            <div class="col-md-5">
                                <!-- Product Single - Gallery ============================================= -->
                                <div class="product-image">
                                    <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                        <div class="flexslider">
                                            <div class="slider-wrap" data-lightbox="gallery">
                                                <div class="slide" data-thumb="{{$catalog->image}}">
                                                    <a href="{{$catalog->image}}" title="{{$catalog->titles}}" data-lightbox="gallery-item">
                                                        <img src="{{$catalog->image}}" alt="{{$catalog->titles}}">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Product Single - Gallery End -->
                            </div>
                            <div class="col-md-7 product-desc">
                                <!-- Product Single - Short Description ============================================= -->
                                <p>{{$catalog->description}}</p>
                                <!-- Product Single - Short Description End -->
                                <!-- Product Single - Quantity & Cart Button ============================================= -->
                                <form id="form_cart">
                                    <input type="hidden" name="catalog" value="{{$catalog->id}}">
                                    <div class="cart mb-0 d-flex justify-content-between align-items-center">
                                        <div class="product-price">
                                            Harga Siang :
                                            Rp. {{number_format($catalog->price_s)}} <br>
                                            Harga Malam :
                                            Rp. {{number_format($catalog->price_m)}}
                                        </div>
                                        @auth
                                        <button onclick="handle_upload('#tombol_keranjang','#form_cart','{{route('user.order.store')}}','POST','Pesan');" id="tambah_keranjang" class="add-to-cart button m-0">Pesan</button>
                                        @endauth
                                        @guest
                                        <a href="{{route('user.auth.index')}}" class="add-to-cart button m-0">Pesan</a>
                                        @endguest
                                    </div>
                                    <!-- Product Single - Quantity & Cart Button End -->
                                    <div class="line"></div>
                                    <div class="row align-items-center">
                                        <div class="col-sm-6 mb-3">
                                            <h5 class="fw-medium mb-3">Tanggal:</h5>
                                            <input type="text" id="tanggal" name="tanggal" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-3 mb-3">
                                            <h5 class="fw-medium mb-3">Jam:</h5>
                                            <select class="form-control" id="jam" name="jam"></select>
                                        </div>
                                        <div class="col-sm-3 mb-3">
                                            <h5 class="fw-medium mb-3">Harga:</h5>
                                            <input type="text" id="harga" name="harga" class="form-control" value="0" readonly>
                                        </div>
                                        <div class="col-sm-9 mb-3">
                                            <h5 class="fw-medium mb-3">Jumlah Jam:</h5>
                                            <div class="quantity clearfix">
                                                <input type="button" value="-" class="minus">
                                                <input type="number" maxlength="1" id="total_jam" step="1" min="1" name="total_jam" value="1" title="Total Jam" class="qty" />
                                                <input type="button" value="+" class="plus">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 mb-3">
                                            <h5 class="fw-medium mb-3">Total Harga:</h5>
                                            <input type="text" id="total_harga" name="total_harga" readonly value="0" class="form-control">
                                        </div>
                                        <div class="col-sm-6 mb-3">
                                            <h5 class="fw-medium mb-3">
                                                Harap transfer ke bank BCA<br>
                                                Transfer ke Rekening : xxx-xxx-xxx-xxx<br>
                                                Atas Nama : Nama<br>
                                                Selain no rek diatas, jangan pernah melakukan transfer
                                            </h5>
                                        </div>
                                        <div class="col-sm-6 mb-3">
                                            <h5 class="fw-medium mb-3">Bukti TF:</h5>
                                            <input name="photo" type="file" accept="image/*" class="file-loading image_picker" data-allowed-file-extensions='[]' data-show-preview="false">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @section('custom_js')
        <script type="text/javascript">
            load_list(1);
            date('tanggal');
            var html = '';
            var sekarang = new Date();
            var day = sekarang.getDate();
            var jam_sekarang = sekarang.getHours();
            var jam_buka = 8;
            var max_jam = 23;
            var tanggal;
            $('#tanggal').datepicker().on('changeDate', function (ev) {
                $("#jam option").remove();
                $("#harga").val(0);
                $("#total_harga").val(0);
                tanggal = $("#tanggal").val();
                $('#jam').append($("<option></option>").attr("value", '').text('Harap pilih jam'));
                if (day == tanggal.substring(0, 2)) {
                    if(jam_sekarang == max_jam){
                        info_toastr('Sudah tutup, harap pilih tanggal lain');
                        $("#tanggal").val('');
                    }else{
                        for (i = jam_sekarang; i < max_jam; i++) {
                            // $(this).append($("<option>").text(i).val(i));
                            $('#jam').append($("<option></option>").attr("value", i).text(i));
                        }
                    }
                }else{
                    for (i = jam_buka; i <= max_jam; i++) {
                        $('#jam').append($("<option></option>").attr("value", i).text(i));
                    }
                }
            });
            $('#jam').on('change', function() {
                if(this.value < 18){
                    $("#harga").val("{{number_format($catalog->price_s)}}");
                }else{
                    $("#harga").val("{{number_format($catalog->price_m)}}");
                }
                var harga = $("#harga").val();
                harga = harga.replace(',','');
                var total_harga = harga * $("#total_jam").val();
                $("#total_harga").val(thousand(total_harga));
            });
            $('#total_jam').on('change', function() {
                var harga = $("#harga").val();
                harga = harga.replace(',','');
                var total_harga = harga * this.value;
                $("#total_harga").val(thousand(total_harga));
            });
        </script>
    @endsection
</x-user-layout>