<script src="{{asset('semicolon/js/jquery.js')}}"></script>
<script src="{{asset('semicolon/js/plugins.min.js')}}"></script>

<!-- Footer Scripts ============================================= -->
<script src="{{asset('semicolon/js/components/bs-filestyle.js')}}"></script>
<!-- Date & Time Picker JS -->
<script src="{{asset('semicolon/js/components/moment.js')}}"></script>
<script src="{{asset('semicolon/js/components/timepicker.js')}}"></script>
<script src="{{asset('semicolon/js/components/datepicker.js')}}"></script>

<!-- Include Date Range Picker -->
<script src="{{asset('semicolon/js/components/daterangepicker.js')}}"></script>
<script src="{{asset('semicolon/js/functions.js')}}"></script>
<script type="text/javascript">
$(".image_picker").fileinput({
    mainClass: "input-group-md",
    showUpload: false,
    previewFileType: "image",
    browseClass: "btn btn-success",
    browseLabel: "Pick Image",
    browseIcon: "<i class=\"icon-picture\"></i> ",
    removeClass: "btn btn-danger",
    removeLabel: "Delete",
    removeIcon: "<i class=\"icon-trash\"></i> ",
    uploadClass: "btn btn-info upload_bukti_tf",
    uploadLabel: "Upload",
    uploadIcon: "<i class=\"icon-upload\"></i> "
});
</script>
<script src="{{asset('js/toastr.js')}}"></script>
<script src="{{asset('js/confirm.js')}}"></script>
<script src="{{asset('js/plugin.js')}}"></script>
<script src="{{asset('js/method.js')}}"></script>