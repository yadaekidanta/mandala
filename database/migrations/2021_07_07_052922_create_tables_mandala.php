<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablesMandala extends Migration
{
    public function up()
    {
        Schema::create('catalog', function (Blueprint $table) {
            $table->id();
            $table->string('titles');
            $table->string('slug');
            $table->longText('description');
            $table->string('price_s',20);
            $table->string('price_m',20);
            $table->string('photo')->nullable();
            $table->timestamps();
        });
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('catalog_id');
            $table->string('notes');
            $table->timestamp('start');
            $table->timestamp('end');
            $table->string('photo');
            $table->string('st');
            $table->string('total',20)->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('catalog');
        Schema::dropIfExists('order');
    }
}
